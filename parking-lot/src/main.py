from models.parking import ParkingLot


if __name__ == "__main__":
    #Instantiate a parking lot with 3 levels of parking (ie 3 floors):
    parking_lot = ParkingLot(3)
    #For each level, give the number of empty spots left:
    for level in parking_lot:
        level.show_availability()

    #Create a list of customers and their vehicles

