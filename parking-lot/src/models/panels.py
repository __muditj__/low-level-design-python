from abc import ABC, abstractmethod
from models import Vehicle
from customer import Customer

class InformationPanel(ABC):

    @abstractmethod
    def show_information(self) -> None:
        """Shows some information"""
        pass



class PaymentPanel(InformationPanel):
    """Panel where payment can be made"""
    
    @abstractmethod
    def make_payment(self, vehicle: Vehicle):
        pass

class EntryPanel(InformationPanel):
    """ Entry panel displays information about the overall state of the parking lot"""
    def __init__(self):
        self.is_space_available = True

    def show_information(self):
        if self.is_space_available:
            return f'Welcome to the Parking Lot. Please take a parking ticket'
        else:
            return f'Sorry there is no space currently available'

    def give_ticket(self, customer: Customer):
        """Set the ticket boolean for the customer instance"""
        if self.is_space_available:
            customer.parking_ticket = True 
            customer.has_paid_fee = False 



class ExitPanel(InformationPanel):
    pass


class GeneralInfoPanel(InformationPanel):
    """Shows information specific to the particular parking lot floor and allows for """
    pass 