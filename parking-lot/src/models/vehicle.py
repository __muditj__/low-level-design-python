from dataclasses import dataclass
from enum import Enum


#Enum defined to hold a list of related constants
class VehicleType(Enum):
    RegularCar: str = "Regular Car",
    ElectricCar: str = "Electric car",
    Truck: str = "Truck"


@dataclass
class Vehicle:
    """ A vehicle can be a car, truck, van, motorcycle etc """
    vehicle_type: VehicleType
    
    