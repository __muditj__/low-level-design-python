from dataclasses import dataclass
from models import Vehicle, ParkingSpot

@dataclass
class Customer:
    parking_ticket: bool = None
    has_paid_fee: bool = None 
    vehicle: Vehicle
    pending_amount: float = 0

    def accept_payment(self):
        """Accepts the pending parking fee and updates attributes accordingly"""
        self.has_paid_fee = True
        self._pending_amount = 0



