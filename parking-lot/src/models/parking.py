from enum import Enum
from datetime import datetime, timedelta
from models.vehicle import VehicleType
from typing import Dict, List

#The top level class in our domain - a parking lot is made up of multiple parkinglotfloor instances
#Can the builder/factory design pattern be incorporated here?
class ParkingLot:
    def __init__(self, levels: int):
        self.entry_point = None
        self.exit_point = None
        self.levels = levels
        self._floors: List[ParkingLotFloor] = None
        self._index = 0
        self._initialize_parking_lot_floors(self.levels)

    def __iter__(self):
        self._index = 0
        return self 
    
    def __next__(self):
        if self._index < len(self._floors):
            value = self._floors[self._index]
            self._index += 1
            return value 
        else:
            raise StopIteration


    def _initialize_parking_lot_floors(self, count: int) -> None:
        self._floors = [ParkingLotFloor() for _ in range(count)]

    @property
    def floors(self):
        return self._floors
    


class ParkingLotFloor:
    """Each parking lot floor has an entry and exit point, some parking spots, and a display board"""
    def __init__(self):
        self.display_board = None 
        self.parking_spots : Dict[VehicleType, List[ParkingSpot]] = {vehicle_type: [] for vehicle_type in VehicleType}

        self._set_parking_spots()
    
    def show_availability(self):
        """ Show availability of parking spots on the display board"""
        print(f'------------------------------------------')
        for vehicle_type in self.parking_spots.keys():
            available_spots = sum(1 for spot in self.parking_spots[vehicle_type] if not spot.is_occupied)
            print(f'For vehicle type {vehicle_type}, there are {available_spots} number of empty parking spots')

    def check_vehicle_availabilty(self, vehicle_type: VehicleType) -> bool:
        """ Checks availability for a particular type of vehicle"""
        return True if not all(spot.is_occupied for spot in self.parking_spots[vehicle_type]) else False
    
    def _set_parking_spots(self):
        for vehicle_type in VehicleType:
            #print(vehicle_type, vehicle_type.value)
            #Create 10 parking spots for each vehicle type and make two of each as a handicapped spot
            for _ in range(8):
                self.parking_spots[vehicle_type].append(ParkingSpot(vehicle_type))
            for _ in range(2):
                self.parking_spots[vehicle_type].append(ParkingSpot(vehicle_type, is_handicapped= True))
            


        


class ParkingSpot:
    """ A spot for parking a vehicle """

    def __init__(self, vehicle_type: VehicleType, is_handicapped: bool = False):
        self._is_handicapped = is_handicapped
        self.vehicle_type = vehicle_type 
        self.is_occupied = False 
        self._occupied_start_time = None
    
    def is_occupied(self) -> bool:
        return self._is_occupied

    
    def occupied_time(self) -> timedelta:
        return self._occupied_start_time
    
    
    def is_handicapped(self) -> bool:
        return self._is_handicapped

    def mark_occupied(self, vehicle: VehicleType) -> bool:
        if vehicle != self.vehicle_type or self.is_occupied:
            return False
        else:
            self._is_occupied = True 
            self._occupied_start_time = datetime.now()

    def mark_unoccupied(self) -> timedelta:
        if not self.is_occupied:
            return timedelta(0)
        else:
            self.is_occupied = False 
            time_difference = datetime.now() - self._occupied_start_time
            self._occupied_start_time = None
            return time_difference
    